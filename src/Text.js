import React from "react";

function Text(props) {
  return (
    <span style={{color:props.color}}>{props.name}!</span>       
  );
}
export default Text;