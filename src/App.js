import React from "react";
import "./App.css";
import Text from "./Text.js";


function App() {
  return (
    <div className="App">
      <h1>
          Hello, <Text color="blue" name="Rafael" />
      </h1>
    </div>
  );
}

export default App;
